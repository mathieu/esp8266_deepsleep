#include <ESP8266WiFi.h>
extern "C" {
#include "user_interface.h" // Required for wifi_station_connect() to work
}

#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <DHT.h>

// if bigger than 4295, modem will wake up anyway? c.f. forceSleepBegin.
// https://www.espressif.com/sites/default/files/documentation/2c-esp8266_non_os_sdk_api_reference_en.pdf
// say no
// should stick on 64bits when mul by 1000000 -> shoudl be smaller than ~18000.10^9 ~580000year
#define SLEEP_TIME_SECOND 10*60


#define DHTTYPE DHT11
#define DHTPIN D3
#define DHT_POWER_PIN 13
DHT dht(DHTPIN, DHTTYPE);

#define FPM_SLEEP_MAX_TIME 0xFFFFFFF // Could be replace by ESP.deepSleepMax() -> https://thingpulse.com/max-deep-sleep-for-esp8266/

#define MQTT_SERVER "192.168.0.250"
#define MQTT_USERNAME "XXXXXX"
#define MQTT_PASSWD "XXXXX"
#define MQTT_PORT 1883
#define TEMPERATURE_FEED_FORMAT "/feeds/mathieu/portable/temperature"
#define HUMIDITY_FEED_FORMAT "/feeds/mathieu/portable/humidity"
#define VOLTAGE_FEED_FORMAT "/feeds/mathieu/portable/voltage"

WiFiClient client;
Adafruit_MQTT_Client *mqtt;
Adafruit_MQTT_Publish *mqtt_temp;
Adafruit_MQTT_Publish *mqtt_pressure;
Adafruit_MQTT_Publish *mqtt_voltage;

const char *ssid     = "XXXXX";
const char *password = "XXXXX";
uint8_t bssid[6] = {0x14, 0x0C, 0x76, 0xB1, 0x37, 0x32};
int32_t channel = 11; //0 for Auto

ADC_MODE(ADC_VCC);
void setup()
{
  // Power on DHT11
  pinMode(DHT_POWER_PIN, OUTPUT);
  //digitalWrite(DHT_POWER_PIN, HIGH);
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.print("\nRunning\n");

}

// ESP.deepSleep( X, WAKE_RF_DISABLED ); disable the radio interface and it can't be
// activated after
// So use WAKE_RF_DEFAULT and play with wifi stat manually
void WiFiOn() // vs WiFi.forceSleepWake();
{

  wifi_fpm_do_wakeup();
  wifi_fpm_close();

  Serial.println("Reconnecting");
  wifi_set_opmode(STATION_MODE);
  wifi_station_connect();
}

int WiFiOff() // vs WiFi.forceSleepBegin();
{

  // Serial.println("diconnecting client and wifi");
  //client.disconnect();
  wifi_station_disconnect();
  wifi_set_opmode(NULL_MODE);
  wifi_set_sleep_type(MODEM_SLEEP_T);
  wifi_fpm_open();
  return wifi_fpm_do_sleep(FPM_SLEEP_MAX_TIME);
}

int mqttConnect(float h, float t)
{
  mqtt		= new Adafruit_MQTT_Client(&client, MQTT_SERVER, MQTT_PORT,
                                     MQTT_USERNAME, MQTT_USERNAME, MQTT_PASSWD);
  uint8_t retries = 3;
  Serial.println("Connecting to mqtt");
  while ((mqtt->connect()) != 0) { // connect will return 0 for connected
    mqtt->disconnect();
    delay(100); // wait
    retries--;
    if (retries == 0) {
      Serial.println("Cannot connect to mqtt");
      return -1;
    }
  }
  Serial.println("Sending mqtt info");
  mqtt_temp     = new Adafruit_MQTT_Publish(mqtt, TEMPERATURE_FEED_FORMAT, 0);
  mqtt_pressure = new Adafruit_MQTT_Publish(mqtt, HUMIDITY_FEED_FORMAT, 0);
  mqtt_voltage = new Adafruit_MQTT_Publish(mqtt, VOLTAGE_FEED_FORMAT, 0);
  if (!mqtt_temp->publish(t)) {
    Serial.println("failed to send temp");
  }
  mqtt_pressure->publish(h);
  mqtt_voltage->publish(ESP.getVcc());
  mqtt->disconnect();
  return 0;
}

int doMeasure()
{
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return -1;
  }

  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.print(" *C ");

  WiFiOn();
  //WiFi.forceSleepWake();
  IPAddress ip(192, 168, 0, 251);
  IPAddress gateway(192, 168, 0, 254);
  IPAddress subnet(255, 255, 255, 0);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password, channel, bssid);

  int retry = 10;
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    if (retry < 0)
      continue;
    retry --;
  }


  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  mqttConnect(h, t);
  if (WiFiOff()) {
    //if (WiFi.forceSleepBegin(0)) {
    Serial.println("Fail to set sleep mode");
  }
  return 0;
}

void loop()
{
  // Wait for DHT to be ready
  delay(2000);
  doMeasure();
  ESP.deepSleep(SLEEP_TIME_SECOND * 1000000, WAKE_RF_DEFAULT);
}
